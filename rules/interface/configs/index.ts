import { JSXElementConstructor } from "react";



export interface IConfig {
    layout: JSXElementConstructor<any>,
    context?: any,
    settings: {
        layout: {
            config: {},
        },
    },
}