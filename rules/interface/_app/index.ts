import { NextPage } from "next"
import { IConfig } from '../configs/index';



export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
    config: IConfig
}

export type AppPropsWithLayout = {
    Component: NextPageWithLayout,
    pageProps: any
}