

export interface IPropsGetCounteries {
    params?: {
        fields?: string
    },
    url: string
}

export interface IPropsGetCounteriesByName {
    name: string,
    params?: {}
}