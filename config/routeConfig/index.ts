import Layout from "layout";
import { IConfig } from "rules/interface/configs"

// contexts
import homeContext from "context/home/context"
import detailContext from "context/details/context"


// Main page path on "/" url
export const MainPageConfig: IConfig = {
  layout: Layout,
  context: homeContext,
  settings: {
    layout: {
      config: {},
    },
  },
};

// counteries detail page path on "/[name]" url
export const DetailsPageConfig: IConfig = {
  layout: Layout,
  context: detailContext,
  settings: {
    layout: {
      config: {},
    },
  },
};

// Error page
export const ErrorPageConfig: IConfig = {
  layout: Layout,
  settings: {
    layout: {
      config: {},
    },
  },
};