FROM node:14.18.1-alpine

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

COPY . .

RUN npx next build

EXPOSE 3000

CMD npx next start -p 3000  
