import React, { createContext, useState } from "react";
import { ValuesState } from "./states";
import { ReactNode } from 'react';


interface IProps {
    children: ReactNode
}

interface IContextValue {
    values?: any,
    setValues?: any
}

export const HomeContextProvider = createContext<IContextValue>({});

export default function homeContext({ children }: IProps) {

    const [values, setValues] = useState(ValuesState)

    return (
        <HomeContextProvider.Provider value={{ values, setValues }}>
            {children}
        </HomeContextProvider.Provider>
    )
}