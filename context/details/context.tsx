import React, { createContext, useState } from "react";
import { ValuesState } from "./states";
import { ReactNode } from 'react';


interface IProps {
    children: ReactNode
}

interface IContextValue {
    values?: any,
    setValues?: any
}

export const DetailsContextProvider = createContext<IContextValue>({});

export default function detailsContext({ children }: IProps) {

    const [values, setValues] = useState(ValuesState)

    return (
        <DetailsContextProvider.Provider value={{ values, setValues }}>
            {children}
        </DetailsContextProvider.Provider>
    )
}