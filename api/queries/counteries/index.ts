import { instance } from "api/instance"
import { AxiosResponse } from "axios"
import { IPropsGetCounteries, IPropsGetCounteriesByName } from "rules/interface/queries/counteries";


export const getCounteries = async ({ params, url }: IPropsGetCounteries): Promise<AxiosResponse> => {
  return await instance.get(
    url, { params }
  ).then(res => res.data)
};

export const getCounteriesByName = async ({ name, params }: IPropsGetCounteriesByName): Promise<AxiosResponse> => {
  return await instance.get(
    "name/" + name, { params }
  ).then(res => res.data)
};