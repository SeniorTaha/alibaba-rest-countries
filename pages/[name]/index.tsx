import Button from "components/button"
import { useCallback, ReactElement, useEffect, useContext } from 'react';
import { NextPageWithLayout } from 'rules/interface/_app';
import { DetailsPageConfig } from 'config/routeConfig';
import { useRouter } from "next/router";
import { useGetCounteriesByName } from '../../hooks/queries/useCounteries/index';
import { DetailsContextProvider } from '../../context/details/context';
import ReactGA from 'react-ga';
import CountryDetailsBody from "components/counteries/detail"



const CounterieDetails: NextPageWithLayout = (): ReactElement => {

    const { setValues } = useContext(DetailsContextProvider)

    const router = useRouter()

    const { data, isLoading, error, isSuccess } = useGetCounteriesByName({
        name: router.query?.name as string,
        params: {
            fullText: true,
            fields: "flags,borders,nativeName,population,region,name,subregion,capital,topLevelDomain,currencies,languages"
        }
    })

    // back btn handler
    const backClickHandler = useCallback((): void => {
        ReactGA.event({
            category: 'Back',
            action: 'Click',
            label: 'detail-back-btn'
        });

        router.back();
    }, [])

    useEffect(() => {
        if (isSuccess) setValues(data)
    }, [isSuccess, data])

    return (
        <>
            <Button onClick={backClickHandler}>
                <svg width="15px" height="15px" viewBox="0 0 200 200" data-name="Layer 1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"><title /><path d="M160,89.75H56l53-53a9.67,9.67,0,0,0,0-14,9.67,9.67,0,0,0-14,0l-56,56a30.18,30.18,0,0,0-8.5,18.5c0,1-.5,1.5-.5,2.5a6.34,6.34,0,0,0,.5,3,31.47,31.47,0,0,0,8.5,18.5l56,56a9.9,9.9,0,0,0,14-14l-52.5-53.5H160a10,10,0,0,0,0-20Z" /></svg>
                Back
            </Button>

            {
                isLoading ?
                    <>loading</>
                    : isSuccess ?
                        <CountryDetailsBody />
                        :
                        <>{error}</>
            }

        </>
    )
}


// export const getServerSideProps: GetServerSideProps = async (params) => {
//     params.res.setHeader(
//       'Cache-Control',
//       'public, s-maxage=3600, stale-while-revalidate=7200'
//     )

//     const { data } = await instance.get(GET_ALL_COUNTERIES_URL);

//     if (!data) {
//       return {
//         props: {
//           error: true
//         }
//       }
//     }
//     return {
//       props: {
//         countries: data
//       }
//     };
//   };

CounterieDetails.config = DetailsPageConfig

export default CounterieDetails