import { MainPageConfig } from 'config/routeConfig'
import { NextPageWithLayout } from 'rules/interface/_app';
import Counteries from 'components/counteries';
import { useGetCounteries } from 'hooks/queries/useCounteries';
import { useContext, useEffect, useState } from 'react';
import { HomeContextProvider } from '../context/home/context';
import { useRouter } from 'next/router';


const Home: NextPageWithLayout = () => {

  const router = useRouter()

  const { setValues } = useContext(HomeContextProvider)

  const [param, setParm] = useState<string>();

  const counteries = useGetCounteries({
    url: (param ? `/region/${param}` : "all"),
    params: {
      fields: "flags,name,region,population,capital"
    }
  });


  // set initial filter
  useEffect(() => {
    if (router.query?.filters) {
      const filters = router.query?.filters ? JSON.parse(router.query?.filters as string) : {}
      if (filters.region) setParm(filters.region)
    }
  }, [router.query])

  // set res value into context
  useEffect(() => {
    if (counteries.isSuccess) {
      setValues(counteries)
    }
  }, [counteries.isSuccess, counteries.data])



  return (
    <>
      <Counteries />
    </>
  )
}


Home.config = MainPageConfig

export default Home