import { ErrorPageConfig } from "../config/routeConfig";

function Error({ statusCode }) {
  return (
    <div className="bg-white c-bg p-xxl m-auto border-r-secondary max-w-400">
      {statusCode === 404 ?
        <h4 className="align-center">متاسفانه صفحه مورد نظر شما یافت نشد</h4>
        :
        <h4 className="align-center">متاسفانه در ارائه خدمات سیستم با مشکل مواجه شده است، لطفا دوباره تلاش نمایید</h4>
      }
    </div>
  )
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
}

Error.config = ErrorPageConfig;

export default Error
