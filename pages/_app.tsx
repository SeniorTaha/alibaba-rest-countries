import '../styles/globals.css'
import { AppPropsWithLayout } from '../rules/interface/_app';
import { Hydrate, QueryClient, QueryClientProvider } from 'react-query';
import { useState } from 'react';

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  
  const [queryClient] = useState(() => new QueryClient({
    defaultOptions: {
      queries: {
        retry: 3, // retry after 3times error
        suspense: false,
        refetchOnReconnect: true,
        refetchOnWindowFocus: false
      },
    }
  }));

  const Layout = Component.config.layout

  return (
    <QueryClientProvider client={queryClient}>
      <Hydrate state={pageProps.dehydratedState}>
        <Layout config={Component.config}>
          <Component {...pageProps} />
        </Layout>
      </Hydrate>
    </QueryClientProvider>
  )
}

export default MyApp
