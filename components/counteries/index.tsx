import { memo, ReactElement } from 'react';
import dynamic from 'next/dynamic';
import CounteriesList from "./list"
const CounteriesFilter = dynamic(() => import("./filters"))



const Counteries = (): ReactElement => {
    return (
        <>
            <CounteriesFilter />
            <CounteriesList />
        </>
    )
}

export default memo(Counteries)