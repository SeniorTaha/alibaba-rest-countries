import DropDown from "components/drop-down"
import Search from "components/search"
import { memo } from "react"
import { regions } from "constants/regions"



const CounteriesFilter = () => {
    return (
        <section className="flex sm:flex-col justify-between items-stretch w-100">
            <Search />
            <DropDown list={regions} name="region" />
        </section>
    )
}

export default memo(CounteriesFilter)