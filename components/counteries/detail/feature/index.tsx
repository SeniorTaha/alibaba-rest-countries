import { memo } from "react"
import styles from "./index.module.scss"


interface IProps {
    data: any
}

const Feature = ({ data }: IProps) => {

    if (!data) return <></>
    return (
        <section className={styles.features + " flex"}>
            <ul className={styles.list__features + " flex flex-col"}>
                <li>
                    <span>Native Name: </span> {data.nativeName}
                </li>

                <li>
                    <span>Population: </span> {data.population}
                </li>

                <li>
                    <span>Region: </span> {data.region}
                </li>

                <li>
                    <span>Sub Region: </span> {data.subregion}
                </li>

                <li>
                    <span>Capital: </span> {data.capital}
                </li>
            </ul>

            <ul className={styles.list__features + " flex flex-col"}>
                <li>
                    <span>Top Level Domain: </span> {data.topLevelDomain?.map((res: string) => res)}
                </li>

                <li>
                    <span>Currencies: </span> {data.currencies?.map((res: { name: string }) => res.name)}
                </li>

                <li>
                    <span>Languages: </span> {data.languages?.map((res: { name: string }) => res.name)}
                </li>
            </ul>
        </section>
    )
}

export default memo(Feature)