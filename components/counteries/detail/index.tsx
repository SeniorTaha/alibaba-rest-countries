import Image from "next/image"
import { memo, useContext } from "react"
import styles from "./index.module.scss"
import Tag from "components/tag"
import { DetailsContextProvider } from "context/details/context";
import Feature from "./feature"



const CountryDetailsBody = () => {
    const { values } = useContext(DetailsContextProvider)

    return (
        <section className={styles.country_card + " flex items-center w-100"}>
            <div className={styles.flag_image__card}>
                <Image
                    width={700}
                    height={500}
                    src={values[0]?.flags.png}
                    alt="flag"
                />
            </div>

            <div className={styles.features__card + " flex flex-col"}>
                <div className={styles.detail__features + " flex flex-col"}>
                    <h4 className="text-color weight-800">{values[0]?.name}</h4>

                    <Feature data={values[0]} />
                </div>

                <div className={styles.tag__features + " flex items-center"}>
                    <h6 className="weight-600 text-color">Border Counteries: </h6>

                    <div className="flex items-center flex-wrap">
                        {
                            values[0]?.borders?.map((res: string) =>
                                <Tag key={res}>{res}</Tag>
                            )
                        }
                    </div>
                </div>
            </div>
        </section>
    )
}

export default memo(CountryDetailsBody)