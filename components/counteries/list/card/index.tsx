import Image from 'next/image';
import Link from 'next/link';
import { memo, ReactElement } from 'react';
import styles from "./index.module.scss"


interface IProps {
    data: any
}

const CounteriesCard = ({ data }: IProps): ReactElement => {
    return (
        <Link href={`/${data.name}`}>
            <div className={styles.card + " flex flex-col"}>
                <div className={styles.card__header}>
                    <Image
                        src={data.flags?.png}
                        width={300}
                        height={300}
                        alt="flag"
                    />
                </div>
                <div className={styles.card__body}>
                    <h4 className={styles.title__body}>{data.name}</h4>

                    <ul>
                        <li>
                            <p>Population: </p> <span>{data.population}</span>
                        </li>
                        <li>
                            <p>Region: </p> <span>{data.region}</span>
                        </li>
                        <li>
                            <p>Capital: </p> <span>{data.capital}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </Link>
    )
}

export default memo(CounteriesCard)