import dynamic from 'next/dynamic';
import { memo, ReactElement, useCallback, useContext, useEffect, ReactNode, useState } from 'react';
import styles from "./index.module.scss"
import { HomeContextProvider } from 'context/home/context';
import { useRouter } from 'next/router';
import Fuse from 'fuse.js';
const Card = dynamic(() => import("./card"))


interface IFilterList {
    list: any[],
    filters: {
        search?: string
    }
}

const CounteriesList = (): ReactElement => {
    const router = useRouter()

    const { values } = useContext(HomeContextProvider)

    const [counteries, setCounteries] = useState<any>([])

    const options = {
        // Search in `name` and in `nativeName` array
        keys: ['name', "nativeName"]
      }

    useEffect(() => {
        // set initial values
        if (values.isSuccess) renderList(values.data)
    }, [router.query, values.data])


    const renderList = useCallback((list: []): void => {
        // when some filter is exist
        if (router.query?.filters) {
            const filters = router.query?.filters ? JSON.parse(router.query?.filters as string) : {}
            filterList({ list, filters })
        } else setCounteries(list) // when filter is not exist, set inital state from api
    }, [counteries, router.query])


    const filterList = useCallback(({ list, filters }: IFilterList) => {
        // when search value is exist
        if (filters.search) {
            // search on name field
            const fuse = new Fuse(list, options)
            const result = fuse.search(filters.search)
            return setCounteries(result.map((res) => res.item))
        }
        else return setCounteries(list)
    }, [counteries, router.query])



    if (values.isLoading) return <>loading ...</>
    else if (values.isSuccess) return (
        <ul className={styles.list + ' flex items-center flex-wrap container'}>
            {
                counteries?.map((res: any) =>
                    <li key={res.name}>
                        <Card data={res} />
                    </li>
                )
            }
        </ul>
    )
    else return <>{values.error}</>
}

export default memo(CounteriesList)