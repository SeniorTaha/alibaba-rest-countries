import { memo, ReactNode } from 'react';
import styles from "./index.module.scss"


interface IProps {
    children: ReactNode
}

const Tag = ({ children }: IProps) => {
    return (
        <div className={styles.tag}>{children}</div>
    )
}

export default memo(Tag)