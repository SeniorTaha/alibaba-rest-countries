import { useRouter } from "next/router";
import { memo, useEffect, useState } from "react"
import { removeObjectNullish } from "utils";
import styles from "./index.module.scss"


interface IProps {
    list: string[],
    name: string
}

const DropDown = ({ list, name }: IProps) => {

    const router = useRouter()

    const [isOptionsOpen, setIsOptionsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState(-1);


    useEffect(() => {
        if (router.query?.filters) {
            const filters = router.query?.filters ? JSON.parse(router.query?.filters as string) : {}

            setSelectedOption(list.findIndex(item => item.includes(filters[name])))
        }
    }, [router.query])

    // toggle drop-down
    const toggleOptions = (): void => {
        setIsOptionsOpen(!isOptionsOpen);
    };

    // parse selected value to query string
    const setSelectedThenCloseDropdown = (index: number): void => {
        setSelectedOption(index);
        setIsOptionsOpen(false);

        // add new value to query string value
        let filters = {}
        filters = router.query?.filters && JSON.parse(router.query?.filters as string ?? {})
        const newObj = removeObjectNullish({ ...filters as Object, [name]: list[index] })
        router.push(`${router.asPath.split("?")?.[0]}?filters=${JSON.stringify(newObj)}`)
    };

    // listening on keydown for dropdown option
    const handleKeyDown = (index: number) => (e: HTMLFormElement) => {
        switch (e.key) {
            case " ":
            case "SpaceBar":
            case "Enter":
                e.preventDefault();
                setSelectedThenCloseDropdown(index);
                break;
            default:
                break;
        }
    };

    // close and and move between options with
    // keyboard arrow and escape button
    const handleListKeyDown = (e: any) => {
        switch (e.key) {
            case "Escape":
                e.preventDefault();
                setIsOptionsOpen(false);
                break;
            case "ArrowUp":
                e.preventDefault();
                setSelectedOption(
                    selectedOption - 1 >= 0 ? selectedOption - 1 : list.length - 1
                );
                break;
            case "ArrowDown":
                e.preventDefault();
                setSelectedOption(
                    selectedOption == list.length - 1 ? 0 : selectedOption + 1
                );
                break;
            default:
                break;
        }
    };

    return (
        <div className={styles.wrapper}>
            <div className={styles.container}>
                <button
                    type="button"
                    aria-haspopup="listbox"
                    aria-expanded={isOptionsOpen}
                    className={isOptionsOpen ? styles.expanded : ""}
                    onClick={toggleOptions}
                    onKeyDown={handleListKeyDown}
                >
                    {selectedOption >= 0 ? list[selectedOption] : "Filter By Region"}
                </button>
                <ul
                    className={styles.options + ` ${isOptionsOpen ? styles.show : ""}`}
                    role="listbox"
                    aria-activedescendant={list[selectedOption]}
                    tabIndex={-1}
                    onKeyDown={handleListKeyDown}
                >
                    {list.map((option, index) => (
                        <li
                            key={index}
                            id={option}
                            role="option"
                            aria-selected={selectedOption === index}
                            tabIndex={0}
                            onKeyDown={() => handleKeyDown(index)}
                            onClick={() => setSelectedThenCloseDropdown(index)}
                        >
                            {option}
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );

}

export default memo(DropDown)