import { ButtonHTMLAttributes, memo, ReactNode } from 'react';
import styles from "./index.module.scss"


interface IProps extends ButtonHTMLAttributes<any> {
    children: ReactNode
}

const Button = ({ children, ...rest }: IProps) => {
    return (
        <button className={styles.btn + " btn"} {...rest}>{children}</button>
    )
}

export default memo(Button)