import { FormEvent, memo, useEffect, useState } from "react"
import styles from "./index.module.scss"
import { useRouter } from 'next/router';
import { removeObjectNullish } from "utils"




const Search = () => {

    const router = useRouter()

    const [value, setValue] = useState<string>()

    // submit form handler
    const submitHandelr = (values: FormEvent) => {
        values.preventDefault();
        // take the all fields value from form
        const form = values.target as HTMLFormElement;
        const formData = new FormData(form);
        const { search } = Object.fromEntries(formData);

        // add new value to query string value
        let filters = {}
        filters = router.query?.filters && JSON.parse(router.query?.filters as string ?? {})
        const newObj = removeObjectNullish({ ...filters as Object, search })
        router.push(`${router.asPath.split("?")?.[0]}?filters=${JSON.stringify(newObj)}`)
    }

    useEffect(() => {
        if (router.query?.filters) {
            const filters = router.query?.filters ? JSON.parse(router.query?.filters as string) : {}
            setValue(filters?.search)
        }
    }, [router.query])

    return (
        <>
            <form
                onSubmit={values => submitHandelr(values)}
                className={styles.search_form}
            >
                <label
                    htmlFor="counteries_search"
                    className={styles.search_box + " flex items-center"}
                >
                    <i>
                        <svg version="1.1" width="14px" height="14px" id="Capa_1" x="0px" y="0px"
                            viewBox="0 0 487.95 487.95" >
                            <g>
                                <g>
                                    <path d="M481.8,453l-140-140.1c27.6-33.1,44.2-75.4,44.2-121.6C386,85.9,299.5,0.2,193.1,0.2S0,86,0,191.4s86.5,191.1,192.9,191.1
                                            c45.2,0,86.8-15.5,119.8-41.4l140.5,140.5c8.2,8.2,20.4,8.2,28.6,0C490,473.4,490,461.2,481.8,453z M41,191.4
                                            c0-82.8,68.2-150.1,151.9-150.1s151.9,67.3,151.9,150.1s-68.2,150.1-151.9,150.1S41,274.1,41,191.4z"
                                    />
                                </g>
                            </g>
                        </svg>
                    </i>

                    <input
                        id="counteries_search"
                        placeholder="Search  for a country..."
                        type="text"
                        name="search"
                        value={value}
                        onChange={e => setValue(e.target.value as string)}
                    />
                </label>
            </form>
        </>
    )
}

export default memo(Search)