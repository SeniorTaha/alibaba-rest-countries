/** @type {import('next').NextConfig} */
const withOffline = require('next-offline')

// offline mode config
const offlineConfig = {
  dontAutoRegisterSw: true,
  workboxOpts: {
    runtimeCaching: [
      {
        urlPattern: /.png$/,
        handler: 'CacheFirst'
      },
      {
        urlPattern: /api/,
        handler: 'NetworkFirst',
        options: {
          cacheableResponse: {
            statuses: [0, 200],
            headers: {
              'x-test': 'true'
            }
          }
        }
      }
    ]
  }
}

const nextConfig = {
  // offline mode
  ...withOffline(offlineConfig),

  reactStrictMode: true,
  swcMinify: true,

  // protect of source
  productionBrowserSourceMaps: true,

  // optimize duplicate chunks
  optimization: {
    mergeDuplicateChunks: true,
  },

  // next images
  images: {
    domains: ["flagcdn.com", "upload.wikimedia.org"]
  },

  // remove console
  compiler: {
    removeConsole: process.env.NODE_ENV === 'development' ? false : true,
  },
}

module.exports = nextConfig
