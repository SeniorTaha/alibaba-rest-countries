import { memo } from "react"




const ProvideContext = ({ children, Context }) => {
    return (
        Context ?
            <Context>
                {children}
            </Context>
            :
            children
    )
}


export default memo(ProvideContext)