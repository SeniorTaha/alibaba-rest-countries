import { useQuery } from "react-query";
import { getCounteries, getCounteriesByName } from 'api/queries/counteries';
import { IPropsGetCounteries, IPropsGetCounteriesByName } from "rules/interface/queries/counteries";



export function useGetCounteries(params: IPropsGetCounteries) {
    return useQuery(["counteries", params],
        () => getCounteries(params)
    );
}

export function useGetCounteriesByName(params: IPropsGetCounteriesByName) {
    return useQuery(["counteries", params],
        () => getCounteriesByName(params),
        {
            enabled: Boolean(params.name)
        }
    );
}